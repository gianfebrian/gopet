package main

import (
	"fmt"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gocraft/dbr"
	"github.com/spf13/viper"

	"github.com/gin-gonic/gin"
	"github.com/mattes/migrate"
	"github.com/mattes/migrate/database/mysql"
	_ "github.com/mattes/migrate/source/file"
	"gitlab.com/gianfebrian/gopet/database"
	"gitlab.com/gianfebrian/gopet/pet"
)

func main() {
	// read config file
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Print(err.Error())
	}

	// setting up db and connect to it
	var err error
	database.DBCon, err = dbr.Open("mysql", viper.GetString("database_dsn"), nil)
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(3)
	}

	// migration setup
	driver, err := mysql.WithInstance(database.DBCon.DB, &mysql.Config{})
	if err != nil {
		fmt.Print(err.Error())
	}

	// migrate db. Note: migration resides in database/migrations relative to the project
	m, err := migrate.NewWithDatabaseInstance("file://"+viper.GetString("migration_path"), "gopet", driver)
	if err != nil {
		fmt.Print(err.Error())
	}
	m.Steps(2)

	// init gin with default middleware
	r := gin.Default()

	// default landing
	r.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "running")
	})

	// Default path for
	v01 := r.Group("v0.1")
	petAPI := pet.ApiRepo{}
	pet.RegisterRouterGroup(&petAPI, v01)

	r.Run(":" + viper.GetString("port"))
}
