package pet

import (
	"errors"
	"testing"

	"github.com/gocraft/dbr"
	"github.com/gocraft/dbr/dialect"
	"gitlab.com/gianfebrian/gopet/database"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func setupMockDB() (dbr.Connection, sqlmock.Sqlmock) {
	db, m, err := sqlmock.New()
	if err != nil {
		panic(err)
	}

	con := dbr.Connection{DB: db, Dialect: dialect.MySQL, EventReceiver: &dbr.NullEventReceiver{}}

	database.DBCon = &con

	return con, m
}

func TestCreate(t *testing.T) {
	_, mock := setupMockDB()

	name := string("test")
	age := int64(1)
	photo := string("photourl")
	p := Pet{ID: nil, Name: &name, Age: &age, Photo: &photo}

	mock.ExpectExec("INSERT INTO `pet`").WillReturnResult(sqlmock.NewResult(1, 1))

	rows := sqlmock.NewRows([]string{"id", "name", "age", "photo"}).
		AddRow(1, name, age, photo)
	mock.ExpectQuery("^SELECT (.+) FROM pet").WillReturnRows(rows)

	if err := p.Create(); err != nil {
		t.Errorf("This error was not expected while creating new pet: %s", err)
	}
}

func TestCreateWithInsertFailure(t *testing.T) {
	_, mock := setupMockDB()

	name := string("test")
	age := int64(1)
	photo := string("photourl")
	p := Pet{ID: nil, Name: &name, Age: &age, Photo: &photo}

	mock.ExpectExec("INSERT INTO `pet`").WillReturnError(errors.New("An exec error"))

	rows := sqlmock.NewRows([]string{"id", "name", "age", "photo"}).
		AddRow(1, name, age, photo)
	mock.ExpectQuery("^SELECT (.+) FROM pet").WillReturnRows(rows)

	err := p.Create()
	if err != nil {
		if err.Error() != "An exec error" {
			t.Error("`An exec error` is expected while creating a new pet")
		}

		return
	}

	t.Error("An error is expected while creating a new pet")
}

func TestCreateWithReturningFailure(t *testing.T) {
	_, mock := setupMockDB()

	name := string("test")
	age := int64(1)
	photo := string("photourl")
	p := Pet{ID: nil, Name: &name, Age: &age, Photo: &photo}

	mock.ExpectExec("INSERT INTO `pet`").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectQuery("^SELECT (.+) FROM pet").WillReturnError(errors.New("A returning error"))

	err := p.Create()
	if err != nil {
		if err.Error() != "A returning error" {
			t.Error("`An returning error` is expected while creating a new pet")
		}

		return
	}

	t.Error("An error is expected while creating a new pet")
}

func TestFindById(t *testing.T) {
	_, mock := setupMockDB()

	name := string("test")
	age := int64(1)
	photo := string("photourl")
	p := Pet{ID: nil, Name: &name, Age: &age, Photo: &photo}

	rows := sqlmock.NewRows([]string{"id", "name", "age", "photo"}).
		AddRow(1, name, age, photo)
	mock.ExpectQuery("^SELECT (.+) FROM pet").WillReturnRows(rows)

	if err := p.FindById(1); err != nil {
		t.Errorf("This error was not expected while finding an existing pet: %s", err)
	}
}

func TestFindByIdWithLoadFailure(t *testing.T) {
	_, mock := setupMockDB()
	p := Pet{}

	mock.ExpectQuery("^SELECT (.+) FROM pet").WillReturnError(errors.New("A load error"))

	err := p.FindById(1)
	if err != nil {
		if err.Error() != "A load error" {
			t.Error("A load error is expected while finding an existing pet")
		}

		return
	}

	t.Error("An error is expected while finding an existing pet")
}

func TestFindByIdWithZeroResultFailure(t *testing.T) {
	_, mock := setupMockDB()
	p := Pet{}

	rows := sqlmock.NewRows([]string{"id", "name", "age", "photo"})
	mock.ExpectQuery("^SELECT (.+) FROM pet").WillReturnRows(rows)

	err := p.FindById(1)
	if err != nil {
		if err.Error() != "Not found" {
			t.Error("A not found error is expected while finding an existing pet")
		}

		return
	}

	t.Error("An error is expected while finding an existing pet")
}

func TestUpdateById(t *testing.T) {
	_, mock := setupMockDB()

	name := string("test")
	age := int64(1)
	photo := string("photourl")
	p := Pet{ID: nil, Name: &name, Age: &age, Photo: &photo}

	mock.ExpectExec("UPDATE `pet`").WillReturnResult(sqlmock.NewResult(1, 1))

	rows := sqlmock.NewRows([]string{"id", "name", "age", "photo"}).
		AddRow(1, name, age, photo)
	mock.ExpectQuery("^SELECT (.+) FROM pet").WillReturnRows(rows)

	if err := p.UpdateById(1); err != nil {
		t.Errorf("This error was not expected while creating new pet: %s", err)
	}
}

func TestUpdateByIdWithExecFailure(t *testing.T) {
	_, mock := setupMockDB()

	name := string("test")
	age := int64(1)
	photo := string("photourl")
	p := Pet{ID: nil, Name: &name, Age: &age, Photo: &photo}

	mock.ExpectExec("UPDATE `pet`").WillReturnError(errors.New("An exec error"))

	rows := sqlmock.NewRows([]string{"id", "name", "age", "photo"}).
		AddRow(1, name, age, photo)
	mock.ExpectQuery("^SELECT (.+) FROM pet").WillReturnRows(rows)

	err := p.UpdateById(1)
	if err != nil {
		if err.Error() != "An exec error" {
			t.Error("`An exec error` is expected while creating a new pet")
		}

		return
	}

	t.Error("An error is expected while creating a new pet")
}

func TestUpdateByIdWithReturningFailure(t *testing.T) {
	_, mock := setupMockDB()

	name := string("test")
	age := int64(1)
	photo := string("photourl")
	p := Pet{ID: nil, Name: &name, Age: &age, Photo: &photo}

	mock.ExpectExec("UPDATE `pet`").WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectQuery("^SELECT (.+) FROM pet").WillReturnError(errors.New("A returning error"))

	err := p.UpdateById(1)
	if err != nil {
		if err.Error() != "A returning error" {
			t.Error("`A returning error` is expected while creating a new pet")
		}

		return
	}

	t.Error("An error is expected while creating a new pet")
}

func TestDeleteById(t *testing.T) {
	_, mock := setupMockDB()
	p := Pet{}

	mock.ExpectExec("DELETE FROM `pet`").WillReturnResult(sqlmock.NewResult(1, 1))

	if err := p.DeleteById(1); err != nil {
		t.Errorf("This error was not expected while creating new pet: %s", err)
	}
}

func TestDeleteByIdWithExecFailure(t *testing.T) {
	_, mock := setupMockDB()
	p := Pet{}

	mock.ExpectExec("DELETE FROM `pet`").WillReturnError(errors.New("An exec error"))

	err := p.DeleteById(1)
	if err != nil {
		if err.Error() != "An exec error" {
			t.Error("`An exec error` is expected while creating a new pet")
		}

		return
	}

	t.Error("An error is expected while creating a new pet")
}
