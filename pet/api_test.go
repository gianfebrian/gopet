package pet

import (
	"bytes"
	"errors"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path"
	"reflect"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/gianfebrian/gopet/database"
)

type SimpleErrorStub struct {
	Key   string
	Error error

	ID    *int64  `db:"id" json:"id"`
	Name  *string `db:"name" json:"name"`
	Age   *int64  `db:"age" json:"age"`
	Photo *string `db:"photo" json:"photo"`
}

func (s *SimpleErrorStub) Create() error {
	return s.Error
}

func (s *SimpleErrorStub) FindById(int) error {
	return s.Error
}

func (s *SimpleErrorStub) UpdateById(int) error {
	return s.Error
}

func (s *SimpleErrorStub) DeleteById(int) error {
	return s.Error
}

func TestRetrievePetById(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{}
	r.GET("/pet/:id", RetrievePetById(&s))
	req, err := http.NewRequest(http.MethodGet, "/pet/4", nil)
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusOK {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusOK, w.Code)
	}
}

func TestRetrievePetByIdWithNotFoundError(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{Error: errors.New("Not found")}
	r.GET("/pet/:id", RetrievePetById(&s))
	req, err := http.NewRequest(http.MethodGet, "/pet/4", nil)
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusNotFound {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusNotFound, w.Code)
	}
}

func TestRetrievePetByIdWithAccesserRelatedError(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{Error: errors.New("Accesser-related error")}
	r.GET("/pet/:id", RetrievePetById(&s))
	req, err := http.NewRequest(http.MethodGet, "/pet/4", nil)
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusNotFound {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusNotFound, w.Code)
	}
}

func TestRegisterPet(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{}
	r.POST("/pet", RegisterPet(&s))
	jsonStr := []byte(`{"key": "value"}`)
	req, err := http.NewRequest(http.MethodPost, "/pet", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusOK {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusOK, w.Code)
	}
}

func TestRegisterPetWithJSONBodyParseError(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{}
	r.POST("/pet", RegisterPet(&s))
	form := url.Values{}
	form.Add("key", "value")
	req, err := http.NewRequest(http.MethodPost, "/pet", strings.NewReader(form.Encode()))
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusBadRequest {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusBadRequest, w.Code)
	}
}

func TestRegisterPetWithAccesserRelatedError(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{Error: errors.New("Accesser-related error")}
	r.POST("/pet", RegisterPet(&s))
	jsonStr := []byte(`{"key": "value"}`)
	req, err := http.NewRequest(http.MethodPost, "/pet", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusUnprocessableEntity {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusUnprocessableEntity, w.Code)
	}
}

func TestModifyPet(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{}
	r.PUT("/pet/:id", ModifyPet(&s))
	jsonStr := []byte(`{"key": "value"}`)
	req, err := http.NewRequest(http.MethodPut, "/pet/999", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusOK {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusOK, w.Code)
	}
}

func TestModifyPetWithJSONBodyParseError(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{}
	r.PUT("/pet/:id", ModifyPet(&s))
	form := url.Values{}
	form.Add("key", "value")
	req, err := http.NewRequest(http.MethodPut, "/pet/999", strings.NewReader(form.Encode()))
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusBadRequest {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusBadRequest, w.Code)
	}
}

func TestModifyPetWithAccesserRelatedError(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{Error: errors.New("Accesser-related error")}
	r.PUT("/pet/:id", ModifyPet(&s))
	jsonStr := []byte(`{"key": "value"}`)
	req, err := http.NewRequest(http.MethodPut, "/pet/999", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusUnprocessableEntity {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusUnprocessableEntity, w.Code)
	}
}

func TestRemovePet(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{}
	r.DELETE("/pet/:id", RemovePet(&s))
	req, err := http.NewRequest(http.MethodDelete, "/pet/999", nil)
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusOK {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusOK, w.Code)
	}
}

func TestRemovePetWithAccesserRelatedError(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{Error: errors.New("Accesser-related error")}
	r.DELETE("/pet/:id", RemovePet(&s))
	req, err := http.NewRequest(http.MethodDelete, "/pet/999", nil)
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusUnprocessableEntity {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusUnprocessableEntity, w.Code)
	}
}

func makeUploadRequest(paramName string, uri string) (*http.Request, error) {
	viper.Set("upload_public_dir", path.Join(".", "..", "public"))
	fileContents := []byte{1, 2, 3, 4, 5}
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, _ := writer.CreateFormFile(paramName, "dummy.jpg")
	part.Write(fileContents)
	writer.Close()

	req, err := http.NewRequest(http.MethodPut, uri, body)
	req.Header.Add("Content-Type", writer.FormDataContentType())

	return req, err
}

func TestUploadPetPhoto(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{}
	r.PUT("/pet/:id/photo", UploadPetPhoto(&s))
	req, err := makeUploadRequest("file", "/pet/999/photo")
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusOK {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusOK, w.Code)
	}
}

func TestUploadPetPhotoWithWrongParamName(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{}
	r.PUT("/pet/:id/photo", UploadPetPhoto(&s))
	req, err := makeUploadRequest("should be file", "/pet/999/photo")
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusBadRequest {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusBadRequest, w.Code)
	}
}

func TestUploadPetPhotoWithAccesserRelatedError(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	s := SimpleErrorStub{Error: errors.New("Accesser-related error")}
	r.PUT("/pet/:id/photo", UploadPetPhoto(&s))
	req, err := makeUploadRequest("file", "/pet/999/photo")
	if err != nil {
		t.Errorf("Could not create request: %v\n", err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	if w.Code != http.StatusUnprocessableEntity {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusUnprocessableEntity, w.Code)
	}
}

func TestPetRepo(t *testing.T) {
	p := ApiRepo{}
	pet := Pet{}

	if reflect.TypeOf(p.GetRepo()) != reflect.TypeOf(&pet) {
		t.Fatal("Type mismatch")
	}
}

type MockRepo struct{}

func (m *MockRepo) GetRepo() database.BasicExecer {
	s := SimpleErrorStub{}
	return &s
}

func TestRegisterRouterGroup(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	v01 := r.Group("v0.1")
	m := MockRepo{}
	RegisterRouterGroup(&m, v01)

	// Test GET /v0.1/pet/:id
	reqGet, errGet := http.NewRequest(http.MethodGet, "/v0.1/pet/999", nil)
	if errGet != nil {
		t.Errorf("Could not create request: %v\n", errGet)
	}

	wGet := httptest.NewRecorder()
	r.ServeHTTP(wGet, reqGet)

	if wGet.Code != http.StatusOK {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusOK, wGet.Code)
	}

	jsonStr := []byte(`{"key": "value"}`)

	// Test POST /v0.1/pet
	reqPost, errPost := http.NewRequest(http.MethodPost, "/v0.1/pet", bytes.NewBuffer(jsonStr))
	if errPost != nil {
		t.Errorf("Could not create request: %v\n", errPost)
	}

	wPost := httptest.NewRecorder()
	r.ServeHTTP(wPost, reqPost)

	if wPost.Code != http.StatusOK {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusOK, wPost.Code)
	}

	// Test PUT /v0.1/pet/:id
	reqPut, errPut := http.NewRequest(http.MethodPut, "/v0.1/pet/999", bytes.NewBuffer(jsonStr))
	if errPut != nil {
		t.Errorf("Could not create request: %v\n", errPut)
	}

	wPut := httptest.NewRecorder()
	r.ServeHTTP(wPut, reqPut)

	if wPut.Code != http.StatusOK {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusOK, wPut.Code)
	}

	// Test DELETE /v0.1/pet/:id
	reqDelete, errDelete := http.NewRequest(http.MethodDelete, "/v0.1/pet/999", nil)
	if errDelete != nil {
		t.Errorf("Could not create request: %v\n", errDelete)
	}

	wDelete := httptest.NewRecorder()
	r.ServeHTTP(wDelete, reqDelete)

	if wDelete.Code != http.StatusOK {
		t.Errorf("Expected to get status %d but got %d instead\n", http.StatusOK, wDelete.Code)
	}
}
