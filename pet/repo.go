package pet

import (
	"errors"

	"gitlab.com/gianfebrian/gopet/database"
)

// Pet is dbr-based model as well as for constructing json-based model
type Pet struct {
	ID    *int64  `db:"id" json:"id"`
	Name  *string `db:"name" json:"name"`
	Age   *int64  `db:"age" json:"age"`
	Photo *string `db:"photo" json:"photo"`
}

// Create will store new pet info into db
func (p *Pet) Create() error {
	session := database.DBCon.NewSession(nil)
	result, err := session.InsertInto("pet").Columns("name", "age", "photo").Record(p).Exec()
	if err != nil {
		return err
	}

	lastInsertedId, _ := result.LastInsertId()
	_, errReturning := session.Select("*").From("pet").Where("id = ?", lastInsertedId).Load(&p)

	if errReturning != nil {
		return errReturning
	}

	return nil
}

// FindById will get pet info from db
func (p *Pet) FindById(id int) error {
	session := database.DBCon.NewSession(nil)
	count, err := session.Select("*").From("pet").Where("id = ?", id).Load(&p)

	if err != nil {
		return err
	}

	if count == 0 {
		return errors.New("Not found")
	}

	return nil
}

// UpdateById will modify existing pet info
func (p *Pet) UpdateById(id int) error {
	session := database.DBCon.NewSession(nil)
	builder := session.Update("pet")

	if p.Name != nil {
		builder.Set("name", p.Name)
	}

	if p.Age != nil {
		builder.Set("age", p.Age)
	}

	if p.Photo != nil {
		builder.Set("photo", p.Photo)
	}

	if _, err := builder.Where("id = ?", id).Exec(); err != nil {
		return err
	}

	if _, err := session.Select("*").From("pet").Where("id = ?", id).Load(&p); err != nil {
		return err
	}

	return nil
}

// DeleteById will delete pet info from db
func (p *Pet) DeleteById(id int) error {
	session := database.DBCon.NewSession(nil)
	if _, err := session.DeleteFrom("pet").Where("id = ?", id).Exec(); err != nil {
		return err
	}

	return nil
}
