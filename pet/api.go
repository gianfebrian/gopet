package pet

import (
	"net/http"
	"path"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/gianfebrian/gopet/database"
	reflections "gopkg.in/oleiade/reflections.v1"
)

// ApiRepoer is an interface to inject a repo into API action context
type ApiRepoer interface {
	GetRepo() database.BasicExecer
}

// ApiRepo is a concrecte which specifically will return Pet struct for Pet API action
type ApiRepo struct{}

// GetRepo is an implementation of Repoer interface method
func (p *ApiRepo) GetRepo() database.BasicExecer {
	pet := Pet{}
	return &pet
}

// RegisterRouterGroup will regist pet group path to API version path
func RegisterRouterGroup(r ApiRepoer, router *gin.RouterGroup) {
	router.GET("/pet/:id", RetrievePetById(r.GetRepo()))
	router.POST("/pet", RegisterPet(r.GetRepo()))
	router.PUT("/pet/:id", ModifyPet(r.GetRepo()))
	router.DELETE("/pet/:id", RemovePet(r.GetRepo()))
	router.PUT("/pet/:id/photo", UploadPetPhoto(r.GetRepo()))
}

// RetrievePetById using HTTP GET with parameter id
func RetrievePetById(exec database.BasicExecer) func(c *gin.Context) {
	return func(c *gin.Context) {
		id, _ := strconv.Atoi(c.Param("id"))
		if err := exec.FindById(id); err != nil {
			msg := err.Error()

			if msg != "Not found" {
				msg = "Failed to process requested id"
			}

			c.JSON(http.StatusNotFound, gin.H{"error": msg})
			return
		}
		c.JSON(http.StatusOK, gin.H{"data": exec})

	}
}

// RegisterPet using HTTP POST
func RegisterPet(exec database.BasicExecer) func(c *gin.Context) {
	return func(c *gin.Context) {
		if err := c.BindJSON(&exec); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request"})

			return
		}

		if err := exec.Create(); err != nil {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Failed to process data"})
		}

		c.JSON(http.StatusOK, gin.H{"data": exec})
	}
}

// ModifyPet using HTTP PUT
func ModifyPet(exec database.BasicExecer) func(c *gin.Context) {
	return func(c *gin.Context) {
		id, _ := strconv.Atoi(c.Param("id"))
		if err := c.BindJSON(&exec); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request"})

			return
		}

		if err := exec.UpdateById(id); err != nil {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Failed to process data"})

			return
		}

		c.JSON(http.StatusOK, gin.H{"data": exec})
	}
}

// RemovePet using HTTP DELETE
func RemovePet(exec database.BasicExecer) func(c *gin.Context) {
	return func(c *gin.Context) {
		id, _ := strconv.Atoi(c.Param("id"))

		if err := exec.DeleteById(id); err != nil {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Failed to process data"})

			return
		}

		// using empty struct to keep OK response concistency
		var empty struct{}
		c.JSON(http.StatusOK, gin.H{"data": empty})
	}
}

// UploadPetPhoto will use HTTP PUT
// It requires end client to provide a Pet ID.
// Media upload for both create and update scenarios are simplified for "the purpose of this test"
// first json data including filemeta info (filename) will be saved,
// the actual file will be saved later (whether the process of storing the file will fail or not),
// CREATE and UPDATE should response with the respective pet model including its id which will be used
// in upload process.
// Media filename collision won't be much concern here as well,
// for example bird.jpg (say this bird has blue color) was the first file uploaded when creating pet X
// then later in the future another bird.jpg (say this bird has red color) being uploaded for creating pet Y
// both pet X and pet Y will fetch the same file but pet X will fail to see its true color
// while pet Y will see it as it is
func UploadPetPhoto(exec database.BasicExecer) func(c *gin.Context) {
	return func(c *gin.Context) {
		file, fileErr := c.FormFile("file")
		if fileErr != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Bad media file"})

			return
		}

		// should check whether the file successfully store?
		c.SaveUploadedFile(file, path.Join(viper.GetString("upload_public_dir"), file.Filename))

		reflections.SetField(exec, "Photo", &file.Filename)

		id, _ := strconv.Atoi(c.Param("id"))
		if updateErr := exec.UpdateById(id); updateErr != nil {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Failed to update data"})

			return
		}

		c.JSON(http.StatusOK, gin.H{"data": exec})
	}
}
