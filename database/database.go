package database

import "github.com/gocraft/dbr"

// DBCon will act as globally shared connection instance
var DBCon *dbr.Connection

// BasicExecer is an interface for basic db query execution
type BasicExecer interface {
	Create() error
	FindById(id int) error
	UpdateById(id int) error
	DeleteById(id int) error
}
