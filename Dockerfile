FROM golang:latest

WORKDIR /go/src/gitlab.com/gianfebrian/gopet
COPY . .

RUN go get -u github.com/kardianos/govendor
RUN govendor sync -v
