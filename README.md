[![pipeline status](https://gitlab.com/gianfebrian/gopet/badges/master/pipeline.svg)](https://gitlab.com/gianfebrian/gopet/commits/master)
[![coverage report](https://gitlab.com/gianfebrian/gopet/badges/master/coverage.svg)](https://gitlab.com/gianfebrian/gopet/commits/master)

#### GOPET ####

Simple CRUD Restful API using GO.

Run with docker compose:

`````bash
docker-compose build api
docker-compose up api #to run API server
docker-compose up coverage #to see coverage test summary
`````

Run without docker:
`````bash
go get go get -u github.com/kardianos/govendor
govendor sync -v
govendor install +local
govendor build +local
gopet #run the app binary
`````
