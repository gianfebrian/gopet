#!/usr/bin/env bash

./scripts/wait-for-it.sh mariadb:3306 && \
    govendor install +local && \
    govendor build +local && \
    gopet
